// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBViT62TWs86efuQsYGVsTlJNWLRHMDPY0",
    authDomain: "angular-class-34ce7.firebaseapp.com",
    databaseURL: "https://angular-class-34ce7.firebaseio.com",
    projectId: "angular-class-34ce7",
    storageBucket: "angular-class-34ce7.appspot.com",
    messagingSenderId: "65699783465",
    
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
