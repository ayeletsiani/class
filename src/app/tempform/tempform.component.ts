import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  
  
  temperature:number;
  cities:object[] = [{id:1, name:'non existent'},{id:2, name:'London'},{id:3, name:'Paris'}];
  city:string;
  
  onSubmit(){
    this.router.navigate(['/temperatures', this.temperature, this.city]);
  }
  constructor(private router: Router) { }
 
  ngOnInit() {
  }

}
