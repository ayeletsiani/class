import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
 
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  bookCollection:AngularFirestoreCollection;
  
  /*  getBooks(){
     const bookObservable = new Observable (
      observer => {
        setInterval(
         ()=> observer.next(this.books),5000
        )
      }
     )
     return bookObservable;
    } */
    getBooks(userId:string):Observable<any[]>{
     //return this.db.collection('books').valueChanges({idField:'id'}); 
     this.bookCollection = this.db.collection(`users/${userId}/books`);
     return this.bookCollection.snapshotChanges().pipe(
       map(
        collection => collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        ) 
       )
     )
    }
    
    addBook(userId:string, title:string,author:string){
      const book = {title:title,author:author};
      //this.db.collection('books').add(book);
      this.userCollection.doc(userId).collection('books').add(book); //הוספה של הספר בתוך האוסף של היוזרים

    }
  
    getBook(userId:string, id:string):Observable<any>{
      return this.db.doc(`users/${userId}/books/${id}`).get();
    }
/*
    addBooks(){
      setInterval(
        ()=> this.books.push({title: 'A new book', author:'new author'})
        ,5000)
      
     } 
    */
   deleteBook(userId:string, id:string)
     {
       this.db.doc(`users/${userId}/books/${id}`).delete();
     }
    
   updateBook(userId:string, id:string,title:string,author:string)
    {
      const book ={title:title,author:author};
      this.db.doc(`users/${userId}/books/${id}`).update(book);
    }


    constructor(private db:AngularFirestore) {}
}
