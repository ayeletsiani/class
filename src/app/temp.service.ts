import { map, catchError } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Observable , throwError} from 'rxjs';
import { Injectable } from '@angular/core'; 
import {HttpClient, HttpErrorResponse} from '@angular/common/http';  

@Injectable({
  providedIn: 'root'
})
export class TempService {
  private URL ="http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY ="6088f62a8529c6728cacdcc0174d64a0";
  private IMP ="&units=metric";
  
  searchWeatherData(cityName: String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(
         map(data => this.transformWeatherData(data)),
         catchError(this.handleError)
      )
  }
  
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
  }
  
  
  
  
  private transformWeatherData(data:WeatherRaw):Weather
  
  {
    return {
    name:data.name,
    country:data.sys.country,
    image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
    description: data.weather[0].description,
    temperature: data.main.temp,
    lat:data.coord.lat,
    lon:data.coord.lon

  }

  }

  constructor(private http:HttpClient) { }

}

