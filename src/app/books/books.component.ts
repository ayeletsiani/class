import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books: any;
  books$:Observable<any>;
  userId:string;
  constructor(private booksservice:BooksService, public auth:AuthService) { }

  ngOnInit() {
   /* this.books=this.BooksService.getBooks().subscribe(
    (books) => this.books = books
    )*/
    //this.booksservice.addBooks();
    //this.books$ = this.booksservice.getBooks();
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.booksservice.getBooks(this.userId);
      }
    )
  }
  deleteBook(id:string){
    this.booksservice.deleteBook(this.userId, id);
  }

}
