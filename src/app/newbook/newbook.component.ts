import { User } from './../interfaces/user';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-newbook',
  templateUrl: './newbook.component.html',
  styleUrls: ['./newbook.component.css']
})
export class NewbookComponent implements OnInit {
  [x: string]: any;
  author:string;
  title:string;
  id: string;
  isEdit: boolean = false;
  buttonText:string = "Add book";
  userId:string;
  constructor(private bookservice:BooksService,
              private router: Router, 
              private route:ActivatedRoute, 
              public auth: AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id)
    {
      this.isEdit = true;
      this.buttonText = "Update Book"
      this.bookservice.getBook(this.userId,this.id).subscribe(
        book => {
          console.log(this.title);
          this.author=book.data().author;
          this.title=book.data().title;
        }
      )
    }
      }
    )
    
  }
  onSubmit(){
    if(this.isEdit) {
        this.bookservice.updateBook(this.userId,this.id, this.title,this.author);
        this.router.navigate(['/books'])
      }
    
      else{
        this.bookservice.addBook(this.userId, this.title,this.author);
        this.router.navigate(['/books'])
      }
    
}}
