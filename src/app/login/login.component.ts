import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  email:string;
  password:string;

  
  constructor(private auth: AuthService, private router: Router, private route:ActivatedRoute) { }

  ngOnInit() {
  }
  onSubmit(){
    console.log(this.email)
  this.auth.logIn(this.email,this.password);
  //this.touter.navigate(['/books'])
  }

}
