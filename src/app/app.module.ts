import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';

import { RouterModule, Routes } from '@angular/router';
import { TempformComponent } from './tempform/tempform.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NewbookComponent } from './newbook/newbook.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifyComponent } from './classify/classify.component';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures',      component: TemperaturesComponent },
  { path: 'temperatures/:temp/:city',      component: TemperaturesComponent },
  { path: 'tempform',      component: TempformComponent },
  { path: 'newbook',      component: NewbookComponent },
  { path: 'newbook/:id',      component: NewbookComponent },
  { path: 'signup',      component: SignupComponent },
  { path: 'login',      component: LoginComponent },
  { path: 'docform',      component: DocformComponent },
  { path: 'classify',      component: ClassifyComponent },
  { path: '', redirectTo: '/books' ,pathMatch: 'full'},

];


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    TempformComponent,
    NewbookComponent,
    SignupComponent,
    LoginComponent,
    DocformComponent,
    ClassifyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    MatFormFieldModule, 
    MatSelectModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'class-angular'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )    
  ],
  providers: [AngularFireAuth ],
  bootstrap: [AppComponent]
})

export class AppModule { }




