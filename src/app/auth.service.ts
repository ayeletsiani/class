import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState //פייר בייס ינהל את היוזרים
  }

  signUp(email:string, password:string)
  {
    this.afAuth
        .auth.createUserWithEmailAndPassword(email,password)
        .then(() => {
          this.router.navigate(['/books']);
        }) //"then" הפונקציה 
                                                            //מחזירה פורמיס
                                                            // כמו סאבסקרייב של אובזראבל
  }

 logOut(){
   this.afAuth.auth.signOut().then(res => console.log('Succesful Logout', res));
 }

 logIn(email:string,password:string){
  console.log(email);

   this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
    () => {
    this.router.navigate(['/books']);
  })



}
}
