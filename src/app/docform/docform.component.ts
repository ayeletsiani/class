import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {
  text:string;
  
  constructor(private classify:ClassifyService, private router:Router) { }

  ngOnInit() {
  
  }
  
  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/classify']);
  }

}
